var buttons = require('sdk/ui/button/action');
var tabs = require("sdk/tabs");
var { setTimeout } = require("sdk/timers");
var panels = require("sdk/panel");
var self = require("sdk/self");


var button = buttons.ActionButton({
    id: "readitlater",
    label: "Save URL to Workflowy",
    icon: {
        "16": "./icon-16.png",
        "32": "./icon-32.png",
        "64": "./icon-64.png"
    },
    onClick: handleClick
});

var panel = panels.Panel({
    width: 180,
    height: 60,
    contentURL: self.data.url("panel.html")
});

function generateGuid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

function generateRandomString(len) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < len; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function generateClientTimestamp() {
    if (!Date.now) {
        Date.now = function () {
            return new Date().getTime();
        }
    }

    const clientShift = (Date.now() - 1406483506);
    return clientShift.toString();
}

function showResult(text, mode) {
    //todo make it like a balloon or something
    if (mode == 'error') {

    }
    //... without any user interaction
    panel.show({
        position: button
    });

    setTimeout(function () {
        panel.hide();
    }, 3000)

}


function sendData(data, toUrl) {

    var Request = require("sdk/request").Request;

    Request({
        url: toUrl,
        content: data,
        onComplete: function (response) {

            showResult('url successfully added', 'success');
        }
    }).post();


}

function getStoreUrl(){
    var {Cc, Ci} = require("chrome");
    var prefs = Cc["@mozilla.org/preferences-service;1"]
        .getService(Ci.nsIPrefService);
    prefs = prefs.getBranch("extensions.readitlater.");
    return prefs.getCharPref("storage.url");
}

function setStoreUrl(newValue){
    var {Cc, Ci} = require("chrome");
    var prefs = Cc["@mozilla.org/preferences-service;1"]
        .getService(Ci.nsIPrefService);
    prefs = prefs.getBranch("extensions.readitlater.");
    prefs.setCharPref("storage.url", newValue);
}

function handleClick(state) {
    
    console.log(getStoreUrl());
    
	const wfUrl = getStoreUrl();
    const mostRecentOp = 586657536; //todo retrieve value from init data
    const shareId = wfUrl.substr(wfUrl.lastIndexOf('/') + 1);
    console.log(shareId);
    
	const crossCheckUserId = '901887';//todo how to get it?
    const priority = 0;//todo get current max priority to add line to end of list

    const actionUrl = "https://workflowy.com/push_and_poll";
    const newGuid = generateGuid();
    const clientTimestamp = generateClientTimestamp();
    //const content = gBrowser.contentTitle + ' ' + gBrowser.currentURI.prePath + gBrowser.currentURI.path;
    const content = tabs.activeTab.title + ' ' + tabs.activeTab.url;


    var jsonPacket = [
        {
            "most_recent_operation_transaction_id": mostRecentOp,
            "operations": [
                {
                    "type": "create",
                    "data": {
                        "projectid": newGuid,
                        "parentid": "None",
                        "priority": priority
                    },
                    "client_timestamp": clientTimestamp,
                    "undo_data": {}
                },
                {
                    "type": "edit",
                    "data": {
                        "projectid": newGuid,
                        "name": content
                    },
                    "client_timestamp": clientTimestamp + 1,
                    "undo_data": {
                        "previous_last_modified": clientTimestamp,
                        "previous_name": ""
                    }
                }
            ],
            "share_id": shareId
        }
    ];

    var form = new Object();
    form.client_id = new Date().toISOString();
    form.client_version = '15';
    form.push_poll_id = generateRandomString(8);
    form.push_poll_data = JSON.stringify(jsonPacket);
    form.share_id = shareId;
    form.crosscheck_user_id = crossCheckUserId;

    sendData(form, actionUrl);

}